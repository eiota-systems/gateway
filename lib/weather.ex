defmodule Gateway.Weather do
  use GenServer
  require Logger

  alias Gateway.DataStore
  alias MeteoStick.{EventManager, WeatherStation}

  def start_link(_) do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    EventManager.add_handler()
    {:ok, []}
  end

  def handle_info(%WeatherStation.State{} = weather, state) do
    DataStore.update(:weather, Map.from_struct(weather))
    {:noreply, state}
  end
end
