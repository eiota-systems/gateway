defmodule Gateway.Heartbeat do
  use GenServer
  require Logger

  def check_status do
    GenServer.call(__MODULE__, :check_status)
  end

  def set_status(status) do
    GenServer.cast(__MODULE__, {:set_status, status})
  end

  def start_link(_) do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    :heart.set_callback(__MODULE__, :check_status)
    {:ok, %{status: :ok}}
  end

  def handle_cast({:set_status, status}, state), do: {:noreply, %{state | status: status}}

  def handle_call(:check_status, _from, %{status: status} = state) do
    Logger.info("Heartbeat status: #{status}")
    {:reply, status, state}
  end
end
