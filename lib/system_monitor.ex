defmodule Gateway.SystemMonitor do
  use GenServer
  alias Gateway.Heartbeat
  require Record

  Record.defrecord(:hostent, Record.extract(:hostent, from_lib: "kernel/include/inet.hrl"))

  def start_link(url: url) do
    GenServer.start_link(__MODULE__, url, name: __MODULE__)
  end

  def init(url) do
    Process.send_after(self(), :ping, 11_000)
    {:ok, %{url: url, status: :ok, connected: false}}
  end

  def handle_info(:ping, %{url: url, connected: connected} = state) do
    state =
      with {:ok, ip} <- resolve_addr(url),
           :ok = try_connect(ip, 80) do
        Heartbeat.set_status(:ok)
        %{state | connected: true, status: :ok}
      else
        _ ->
          case connected do
            true ->
              Heartbeat.set_status(:error)
              %{state | status: :error}

            _ ->
              state
          end
      end

    Process.send_after(self(), :ping, 11_000)
    {:noreply, state}
  end

  defp resolve_addr(address) do
    with {:ok, hostent} <- :inet.gethostbyname(to_charlist(address)),
         hostent(h_addr_list: ip_list) = hostent,
         first_ip = hd(ip_list) do
      {:ok, first_ip}
    else
      _ -> {:error, "Error resolving #{address}"}
    end
  end

  defp try_connect(address, port) do
    case :gen_tcp.connect(address, port, []) do
      {:ok, pid} ->
        :gen_tcp.close(pid)
        :ok

      {:error, :econnrefused} ->
        # If the connection is refused, the machine is up.
        :ok

      _error ->
        :error
    end
  end
end
