defmodule Gateway.LightController do
  use GenServer
  require Logger

  alias Gateway.{Light, Switch}
  alias Wampex.Client
  alias Wampex.Roles.Callee.{Register, Yield}
  alias Wampex.Roles.Dealer.Invocation

  def start_link(info) do
    GenServer.start_link(__MODULE__, info, name: __MODULE__)
  end

  def init({id, client}) do
    Client.add(client, self())
    {:ok, %{client: client, id: id}}
  end

  def handle_info({:connected, _}, %{client: client, id: id} = state) do
    register(client, id)
    {:noreply, state}
  end

  def handle_info(
        %Invocation{
          request_id: rid,
          arg_kw: %{"position" => pos}
        },
        %{client: client} = state
      ) do
    pos = Switch.position(Light, pos)
    Client.yield(client, %Yield{request_id: rid, arg_kw: %{position: pos}})
    {:noreply, state}
  end

  defp register(client, id) do
    Client.register(client, %Register{procedure: "agtech_gateway.#{id}.light"})
  end
end
