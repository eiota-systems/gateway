defmodule Gateway.Reporter do
  @moduledoc false
  use GenServer
  require Logger

  alias Gateway.DataStore
  alias Wampex.Client
  alias Wampex.Roles.Caller.Call
  alias Wampex.Roles.Dealer.Result
  alias Wampex.Roles.Publisher.Publish

  def start_link(data) do
    GenServer.start_link(__MODULE__, data, name: __MODULE__)
  end

  def init({id, client}) do
    Client.add(client, self())
    {:ok, %{id: id, client: client, timer: nil, profile: get_profile()}}
  end

  def handle_info(
        {:connected, _},
        %{client: client, id: id, timer: timer, profile: profile} = state
      ) do
    timer = start_reporting(timer)
    do_connect(client, id, profile)
    {:noreply, %{state | timer: timer}}
  end

  def handle_info(:send_data, %{id: id, client: client} = state) do
    data = DataStore.get()
    Logger.info("Publishing: #{inspect(data)}")
    Client.publish(client, %Publish{topic: "agtech_gateway.#{id}", arg_kw: %{id: id, data: data}})
    Process.send_after(self(), :send_data, 5_000)
    {:noreply, state}
  end

  def handle_info(_, state) do
    {:noreply, state}
  end

  defp start_reporting(nil), do: Process.send_after(self(), :send_data, 0)
  defp start_reporting(timer), do: timer

  defp do_connect(client, id, profile) do
    case Client.call(
           client,
           %Call{
             procedure: "device.connect",
             arg_kw: %{id: id, profile: profile}
           }
         ) do
      %Result{} = res ->
        res

      er ->
        Logger.warn("Can't connect to device shadow: #{inspect(er)}")
        :timer.sleep(1000)
        do_connect(client, id, profile)
    end
  end

  defp get_profile do
    :gateway
    |> :code.priv_dir()
    |> Path.join("profile.json")
    |> File.read!()
    |> Jason.decode!()
  end
end
