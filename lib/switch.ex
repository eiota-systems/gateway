defmodule Gateway.Switch do
  use GenServer
  require Logger

  alias Circuits.GPIO

  def position(mod, pos) do
    GenServer.call(mod, {:position, pos})
  end

  def start_link(pin: pin, name: name) do
    GenServer.start_link(__MODULE__, pin, name: name)
  end

  def init(pin) do
    {:ok, gpio} = GPIO.open(pin, :output)
    # Process.send_after(self(), :toggle_switch, 100)
    {:ok, %{gpio: gpio, position: 0}}
  end

  def handle_call({:position, pos}, _, %{gpio: gpio} = state) do
    GPIO.write(gpio, pos)
    {:reply, pos, state}
  end

  def handle_info(:toggle_switch, %{gpio: gpio, position: pos} = state) do
    np = toggle(pos)
    GPIO.write(gpio, np)
    Process.send_after(self(), :toggle_switch, 2000)
    {:noreply, %{state | position: np}}
  end

  defp toggle(1), do: 0
  defp toggle(0), do: 1
end
