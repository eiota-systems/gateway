defmodule Gateway.BoardID do
  @moduledoc false
  require Logger

  def get do
    {id, 0} = System.cmd("/usr/bin/boardid", [])
    id |> String.split("\n") |> List.first()
  rescue
    e in ErlangError ->
      Logger.error("#{inspect(e)}")
      "123456789"
  end
end
