defmodule Gateway.WaterQuality do
  use GenServer
  require Logger

  alias Gateway.DataStore
  alias Circuits.I2C

  @mappings %{
    ec: %{channel: 96},
    temp: %{channel: 97},
    ph: %{channel: 98}
    # do: %{channel: 0x99}
  }
  @read_bytes 32

  defmodule State do
    defstruct [:i2c, sensors: [], values: %{ec: 0, temp: 0, ph: 0, do: 0}]
  end

  def start_link(_) do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    {:ok, i2c} = I2C.open("i2c-1")
    Logger.info("I2C Started: #{inspect(i2c)}")
    Process.send_after(self(), :read_i2c, 0)
    {:ok, %State{i2c: i2c}}
  end

  def handle_info(:read_i2c, %State{i2c: i2c, values: values} = state) do
    values =
      Enum.reduce(@mappings, values, fn {k, %{channel: channel}}, acc ->
        :ok = I2C.write(i2c, channel, "R")

        # most Atlas Scientific stamps take about 1000ms to return a value
        :timer.sleep(1000)
        # Read 32 bytes from i2c channel we wrote to a second ago.
        {:ok, <<_rc::integer, data::binary>>} = I2C.read(i2c, channel, @read_bytes)
        Logger.info("Got #{k} data: #{inspect(data)}")

        val =
          data
          |> parse_ascii("")
          |> parse_value(k)

        vals = Map.put(acc, k, val)
        DataStore.update(:water_quality, vals)
      end)

    # Take a reading every second
    Process.send_after(self(), :read_i2c, 0)
    {:noreply, %State{state | values: values}}
  end

  def parse_ascii(<<>>, final), do: final
  def parse_ascii(<<s::integer, rest::binary>>, final) when s == 0, do: parse_ascii(rest, final)
  def parse_ascii(<<s::integer, rest::binary>>, final), do: parse_ascii(rest, final <> <<s>>)

  def parse_value(val, :ec) do
    [conductivity, tds, salinity, sg] = String.split(val, ",")

    [
      String.to_float(conductivity),
      String.to_integer(tds),
      String.to_float(salinity),
      String.to_float(sg)
    ]
  end

  def parse_value(val, _), do: String.to_float(val)
end
