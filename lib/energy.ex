defmodule Gateway.Energy do
  @moduledoc false
  use GenServer
  require Logger

  alias Gateway.DataStore

  def start_link(ip) do
    GenServer.start_link(__MODULE__, ip, name: __MODULE__)
  end

  def init(ip) do
    Process.send_after(self(), :get_data, 0)
    {:ok, ip}
  end

  def handle_info(:get_data, ip) do
    case get_neurio_data(ip) do
      %Neurio.State{} = data ->
        DataStore.update(:energy, Map.from_struct(data))

      _ ->
        :noop
    end

    Process.send_after(self(), :get_data, 2000)
    {:noreply, ip}
  end

  defp get_neurio_data(ip) do
    case Neurio.get(ip) do
      {:error, %HTTPoison.Error{reason: r}} -> r
      {:ok, %HTTPoison.Response{body: body}} -> body
      _ -> nil
    end
  end
end
