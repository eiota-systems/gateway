defmodule Gateway.WebCam do
  use GenServer
  require Logger

  alias Gateway.CameraController

  def start_link(url: url) do
    GenServer.start_link(__MODULE__, url, name: __MODULE__)
  end

  def init(url) do
    Process.send_after(self(), :get_image, 0)
    default_image = get_default_image()
    {:ok, %{url: url, default_image: default_image}}
  end

  def handle_call(:image, _from, %{image: image} = state) do
    {:reply, image, state}
  end

  def handle_info(:get_image, %{url: url, default_image: di} = state) do
    image = get_image(url, di)
    CameraController.set_image(image)
    Process.send_after(self(), :get_image, 1000)
    {:noreply, state}
  end

  def handle_info(ev, state) do
    Logger.error("#{__MODULE__}: Unknown info event: #{inspect(ev)}")
    {:noreply, state}
  end

  def get_image(url, di) do
    case Mojito.get(url) do
      {:ok, %Mojito.Response{body: body}} ->
        Base.encode64(body)

      _ ->
        di
    end
  end

  def get_default_image do
    :gateway
    |> :code.priv_dir()
    |> Path.join("default_image.png")
    |> File.read!()
    |> Base.encode64()
  end
end
