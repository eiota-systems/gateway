defmodule Gateway do
  @moduledoc false

  use Application
  require Logger

  alias Gateway.{
    BoardID,
    CameraController,
    DataStore,
    Energy,
    Heartbeat,
    LightController,
    PumpController,
    Reporter,
    Switch,
    SystemMonitor,
    WaterQuality,
    Weather,
    WebCam
  }

  alias Wampex.Client
  alias Wampex.Client.Session
  alias Wampex.Client.{Authentication, Realm}
  alias Wampex.Roles.{Callee, Caller, Publisher, Subscriber}

  def start(_type, _args) do
    session = wamp_session()

    children =
      [
        Heartbeat,
        {SystemMonitor, url: get_ping_url()},
        {DataStore, %{water_quality: %{}, energy: %{}, weather: %{}, light: %{}, pump: %{}}},
        {Client, name: Device, session: session, reconnect: true},
        {Energy, neurio_ip()},
        {CameraController, {get_id(), Device}},
        {WebCam, url: web_cam_url()},
        {Reporter, {get_id(), Device}}
      ] ++ children(target())

    opts = [strategy: :one_for_one, name: Gateway.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def children(:host), do: []

  def children(_target) do
    [
      {Weather, []},
      Supervisor.child_spec({Switch, pin: 18, name: Gateway.Light}, id: :light),
      Supervisor.child_spec({Switch, pin: 23, name: Gateway.Pump}, id: :pump),
      {LightController, {get_id(), Device}},
      {PumpController, {get_id(), Device}},
      WaterQuality
    ]
  end

  def target() do
    Application.get_env(:gateway, :target)
  end

  defp get_id, do: BoardID.get()

  defp neurio_ip, do: "192.168.0.119"

  defp web_cam_url, do: "http://192.168.0.159/snapshot.cgi"

  defp get_ping_url, do: "wikipedia.org"

  defp wamp_session do
    url = Application.get_env(:gateway, :wamp_router)
    realm = Application.get_env(:gateway, :realm)
    authid = Application.get_env(:gateway, :authid)
    secret = Application.get_env(:gateway, :secret)

    authentication = %Authentication{
      authid: authid,
      authmethods: ["wampcra"],
      secret: secret
    }

    realm = %Realm{name: realm, authentication: authentication}

    device_roles = [Callee, Caller, Publisher, Subscriber]

    %Session{
      url: url,
      realm: realm,
      roles: device_roles
    }
  end
end
