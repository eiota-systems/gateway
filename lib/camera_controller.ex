defmodule Gateway.CameraController do
  use GenServer
  require Logger

  alias Wampex.Client
  alias Wampex.Roles.Callee.{Register, Yield}
  alias Wampex.Roles.Dealer.Invocation

  def set_image(image) do
    GenServer.cast(__MODULE__, {:set_image, image})
  end

  def get_image do
    GenServer.call(__MODULE__, :get_image)
  end

  def start_link(info) do
    GenServer.start_link(__MODULE__, info, name: __MODULE__)
  end

  def init({id, client}) do
    Client.add(client, self())
    {:ok, %{id: id, client: client, image: nil}}
  end

  def handle_call(:get_image, _from, %{image: image} = state), do: {:reply, image, state}

  def handle_cast({:set_image, image}, state) do
    {:noreply, %{state | image: image}}
  end

  def handle_info({:connected, _}, %{client: client, id: id} = state) do
    register(client, id)
    {:noreply, state}
  end

  def handle_info(
        %Invocation{
          request_id: rid
        },
        %{client: client, image: image} = state
      ) do
    Client.yield(client, %Yield{request_id: rid, arg_kw: %{image: image}})
    {:noreply, state}
  end

  defp register(client, id) do
    Client.register(client, %Register{procedure: "agtech_gateway.#{id}.image"})
  end
end
