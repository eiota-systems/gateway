defmodule Gateway.DataStore do
  use Agent

  def start_link(initial) do
    Agent.start_link(fn -> initial end, name: __MODULE__)
  end

  def get do
    Agent.get(__MODULE__, & &1)
  end

  def update(key, data) do
    Agent.update(__MODULE__, &Map.put(&1, key, data))
    data
  end
end
